﻿using System;

namespace Runner.DAL.Contract
{
    /// <summary>
    /// Common properties for the all entities.
    /// </summary>
    public abstract class ModelBase : IIdentifier
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string ShortName { set; get; }

        public string Abbreviation { get; set; }

        public string Description { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime LastUpdatedAt { get; set; }
    }
}