﻿namespace Runner.DAL.Contract
{
    public interface IIdentifier
    {
        int Id { get; set; }
    }
}