﻿using System.Collections.Generic;
using Runner.DAL.Contract;

namespace Runner.DAL.Model
{
    public class Organization : ModelBase
    {
        public ICollection<Action> Actions { get; set; }
    }
}