﻿using Runner.DAL.Contract;

namespace Runner.DAL.Model
{
    public class ActionGroup : ModelBase
    {
        public Action Action { get; set; }
    }
}