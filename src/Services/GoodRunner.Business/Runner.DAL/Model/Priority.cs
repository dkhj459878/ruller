﻿using System.Collections.Generic;
using Runner.DAL.Contract;

namespace Runner.DAL.Model
{
    public class Priority : ModelBase
    {
        public ICollection<Measure> Measures { get; set; }
    }
}