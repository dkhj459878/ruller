﻿using Runner.DAL.Contract;

namespace Runner.DAL.Model
{
    public class Person : ModelBase
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string MiddleName { get; set; }

        public string Address { get; set; }

        public string MobilePhone { get; set; }

        public string StationaryPhone { get; set; }

        public string Fax { get; set; }

        public string Email { get; set; }
    }
}