﻿namespace Runner.DAL.Model
{
    public class Money
    {
        public Currency Currency { get; set; }

        public int Item { get; set; }

        public int Cent { get; set; }
    }
}