﻿using Runner.DAL.Contract;

namespace Runner.DAL.Model
{
    public class Objective : ModelBase
    {
        public ObjectiveType Type { get; set; }
    }
}