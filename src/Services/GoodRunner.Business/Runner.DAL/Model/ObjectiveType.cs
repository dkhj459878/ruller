﻿using System.Collections.Generic;
using Runner.DAL.Contract;

namespace Runner.DAL.Model
{
    public class ObjectiveType : ModelBase
    {
        public ICollection<Objective> Objective { get; set; }
    }
}