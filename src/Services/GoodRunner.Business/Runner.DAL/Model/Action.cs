﻿using System.Collections.Generic;
using Runner.DAL.Contract;

namespace Runner.DAL.Model
{
    public class Action : ModelBase
    {
        /// <summary>
        /// Background of the action.
        /// </summary>
        public string Background { get; set; }

        /// <summary>
        /// Contribution to the priority and measure.
        /// </summary>
        public string Contribution { get; set; }

        /// <summary>
        /// Problem description.
        /// </summary>
        public string ProblemDescription { get; set; }

        /// <summary>
        /// Novelty of the action.
        /// </summary>
        public string NoveltyOfTheAction { get; set; }

        public ICollection<ActionGroup> Groups { get; set; }

        public Person PersonApplicant { get; set; }

        public Organization OrganizationApplicant { get; set; }

        public ICollection<Organization> Partners { get; set; }

        public ICollection<Objective> OverallObjectives { get; set; }

        public ICollection<Objective> SpecificObjective { get; set; }

        public ICollection<Measure> Measures { get; set; }

        public ICollection<Subject> TargetGroups { get; set; }

        public ICollection<Subject> FinalBeneficiaries { get; set; }

        public ICollection<Result> Results { get; set; }

        public ICollection<Indicator> Indicators { get; set; }

        public Currency TotalBudget { get; set; }
    }
}