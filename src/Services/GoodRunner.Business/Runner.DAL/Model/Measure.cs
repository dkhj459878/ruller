﻿using System.Collections.Generic;
using Runner.DAL.Contract;

namespace Runner.DAL.Model
{
    public class Measure : ModelBase
    {
        public Priority Priority { get; set; }

        public ICollection<Action> Actions { get; set; }
    }
}