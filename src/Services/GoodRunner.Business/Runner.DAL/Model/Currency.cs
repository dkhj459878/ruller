﻿namespace Runner.DAL.Model
{
    public class Currency
    {
        public string CurrencyCode { get; set; }

        public int Number { get; set; }

        public int Cent { get; set; }
     }
}