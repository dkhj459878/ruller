﻿using System.Collections.Generic;
using Runner.DAL.Contract;

namespace Runner.DAL.Model
{
    public class Subject : ModelBase
    {
        public ICollection<Action> Action { get; set; }
    }
}